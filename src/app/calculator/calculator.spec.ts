import { Calculator } from "./calculator";

describe("Suite for Calculator", () => {
  describe("Tests for method multiply", () => {
    
    //un describe por test. Multiplicar dos numeros (3*3)
    it("should return 9", () => {
      //Arrange - Preparar
      //creamos instancia de Calculator
      let calc = new Calculator(); 
      //Act - Actuar
      //Ejecutamos la funcion que queremos (Multiply)
      let result = calc.multiply(3, 3); 
      //Assert - Verificar
      //Validamos que los resultados sean los esperados
      expect(result).toEqual(9); //expect, función de jasmine
    });

    //un describe por test. Cualquier número multiplicado por CERO da CERO
    it("should return 0", () => {
      //Arrange - Preparar
      //creamos instancia de Calculator
      let calc = new Calculator(); 
      //Act - Actuar
      //Ejecutamos la funcion que queremos (Multiply)
      let result = calc.multiply(8, 0); 
      //Assert - Verificar
      //Validamos que los resultados sean los esperados
      expect(result).toEqual(0); //expect, función de jasmine
    });
  });
});
