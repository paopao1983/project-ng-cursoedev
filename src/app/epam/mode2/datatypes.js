//Variales var - let - const

function Vars() {
    var a = 1;
    console.log(a);
}

{
    let b=2;
    console.log(b);
    var c = 3;
    const d = undefined;
    console.log(d);

}
console.log(c);

//Tipos primitivos
/*
string
number
boolean
bigint
undefined
symbol
null
*/
{
    let a="3";
    let b=7;
    console.log(Number(a)+7)
    let hexa = 0b111;

    console.log(hexa)
}

let objPersona = {
    nombre:"Sandra",
    apellido: "Castro",
    edad: 40,
    estatura: 1.56,
    casada: true,
    hijos: {
        hijo1: "Nicolas",
        hijo2: "MianValen" 
    }
};

console.log(objPersona["hijos"]["hijo1"]);
const prop= "nombre"

console.log(objPersona[prop])

//ARRAYS
