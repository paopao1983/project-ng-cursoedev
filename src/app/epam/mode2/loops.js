let objPersona = {
    nombre:"Sandra",
    apellido: "Castro",
    edad: 40,
    estatura: 1.56,
    casada: true,
    hijos: {
        hijo1: "Nicolas",
        hijo2: "MianValen" 
    }
};

for (let key in objPersona){
    console.log(typeof(objPersona[key]));

    if (typeof(objPersona[key]) == "object"){
        console.log()
        for (let key2 in objPersona[key]){
            console.log(key2, objPersona[key][key2]);
        }
    }else{
        console.log(key, objPersona[key]);
    }
}

const objArray=[1,2,3,4,5]

for(let el of objArray){
    console.log(el);
}