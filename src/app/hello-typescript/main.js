//declaracion de una var en Jtsc puro
var mivar = "Mi variable JS";
function miFuncion(x, y) {
    return x + y;
}
//ES 6 de 2015
var num = 2;
var PI = 3.14;
var numeros = [1, 2, 3];
numeros.map(function (valor) {
    return valor * 2;
}); //
numeros.map(function (valor) { return valor * 2; });
var Matematica = /** @class */ (function () {
    function Matematica() {
    }
    Matematica.prototype.suma = function (x1, y) {
        return x1 + y;
    };
    return Matematica;
}());
var n1 = 'asdads';
n1 = 4;

var friends = ["Angie", "Orlando", "Angelica"];
console.log(friends.length);
console.log(friends[1])