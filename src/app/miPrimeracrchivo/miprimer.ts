import { Component } from '@angular/core' //biblioteca de angular 2

@Component({
    selector:'mi-primercomponente',
    template: `
        <p>Mi primer componente con Angular 2</p>
        <h3>Estringona</h3>
    `
})
 export class MiPrimerComponent{ 

 }