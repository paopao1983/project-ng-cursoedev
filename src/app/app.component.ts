import { Component, OnInit } from '@angular/core';
import { Calculator } from './calculator/calculator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']  
})

export class AppComponent {

  title = 'project-ng-cursoedev';
  
   birthday = new Date(1988, 3, 15); // April 15, 1988
   nombre = 'sandra CastrO BAlllesteros';
  //Ejemplo en curso de DesarrolloWeb
  ngOnInit(){
    /**MANUALMENTE - NO PROFESIONAL*/
   /* let calculator = new Calculator();

    
    let result = calculator.multiply(3, 3);
    
    console.log(result === 9);//Comprobaciones de Pasa/No Pasa
    console.log(result !==12)//Comprobaciones de Pasa/No Pasa

    let result2 = calculator.divide(6,2);
    console.log(result2 === 3);
    console.log(result2 !== 34);

    let result3 = calculator.divide(6,0);
    console.log(result3 === null);
  */
    /**IMPLEMENTACION CON KARMA Y JASMINE PROFESIONALMENTE*/
    //Cada archivo de clase debería tener su prueba, es decir
    //su archivo [nombreclase].spec.ts

 

  }
}
