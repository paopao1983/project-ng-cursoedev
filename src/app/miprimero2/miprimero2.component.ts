import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'miprimero2',
  templateUrl: './miprimero2.component.html',
  styleUrls: ['./miprimero2.component.scss']
})
export class Miprimero2Component {

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
    var friends = ["Angie", "Orlando", "Angelica"];
    var edad = 24;
    
   for(let i=0; i<=friends.length-1;i++){
      console.log(friends[i]);
    }

    
    friends.forEach(element =>{
      console.log(element);
    });

    for(let friend of friends){
      console.log("Hello " + friend);
    }

    deleteLast(friends);
    
    friends = ["Angie", "Orlando", "Angelica"];
    
    deleteFirst(friends);

    function deleteLast(friends)
    {
      while(friends.length>0)
      {
        //Remove the lastone in an array
        friends.pop();
        console.log("Now there are:>> " + friends.length);
        console.log("They are:>> " + friends);
      }
    }

    function deleteFirst(friends)
    {
      while(friends.length>0)
      {
        //Remove the firtone in an array
        friends.shift();
        //Interpolate string
        console.log(
          `2. Now there are:>>, ${friends.length} - They are:>> ${friends}`);
      }
    }

  
  }

}
