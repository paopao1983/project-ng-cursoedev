import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Miprimero2Component } from './miprimero2.component';

describe('Miprimero2Component', () => {
  let component: Miprimero2Component;
  let fixture: ComponentFixture<Miprimero2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Miprimero2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Miprimero2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
