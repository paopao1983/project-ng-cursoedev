var fulano = {
    nombre: "Sandra",
    apellido: "Castro",
    edad: 17,
    ingeniero: true,
    cocinero: true,
    cantante: false,
    dj: false,
    fotografo: true
}

function imprimirSiEsMayorDeEdad(persona){
    
    if(persona.edad>=18)
        console.log(`${persona.nombre} es *mayor* de Edad`)
    else
        console.log(`${persona.nombre} es *menor* de Edad`)
}

imprimirSiEsMayorDeEdad(fulano)




function imprimirProfesiones(persona){
    
    console.log(`${persona.nombre} es: `)

    if(persona.ingeniero)
        console.log('Ingeniero')
    if(persona.cocinero)
        console.log('Cocinero')
    if(persona.cantante)
        console.log('Cantante')
    if(persona.dj)
        console.log('Dj')
    if(persona.fotografo)
        console.log('Fotografo')
}

imprimirProfesiones(fulano)