var sandra ={
    nombre: 'Sandra',
    apellido: 'Castro',
    altura: 1.56
}

var alfre ={
    nombre: 'Alfred0',
    apellido: 'Rodriguez',
    altura: 1.87
}

var nico ={
    nombre: 'Nicolas',
    apellido: 'Rodriguez',
    altura: 0.9
}

var personas  = [sandra, alfre, nico]

for(var i = 0; i < personas.length; i++){
    var persona = personas[i]
    console.log(`${persona.nombre} mide ${persona.altura} mtrs o cms`)
}

/**
 * Filtrar Arrays
 */
 const esAlta = ({altura}) => altura >= 1.8
 const esBaja = ({altura}) => !esAlta({altura})
 
 var personasAltas = personas.filter(esAlta)
 
 var personasBajas = personas.filter(esBaja)
 
 console.log('Las personas Altas son: ')
 console.log(personasAltas)
 console.log('Las personas Bajas son: ')
 console.log(personasBajas)


 
 /*var personasAltas = personas.filter(function(persona){
    return persona.altura >= 1.8
 })*/

 //console.log(personasAltas)