********************************************************
*                                                      *
*           J A V A S C R I P T  N O T E S             *
*                                                      *
********************************************************

* FUNCIONES JS *
1. typeof: Funcion de JS que permite conocer el tipo de dato/valor que se está usando.

* Que es Una Variable *
Permiten guardar valores en memoria. Es la representación de un lugar en memoria que 
se utilizará para referirnos a una representación del lugar donde se guardará un Valor.

Ejemplos

var nombre = "Oscar";

Declaración: var nombre2;
Inicialización: nombre2 = "Sandra"

Objetos

Array: 

var elementos = ["Comprar", "Vender", "Alquilar"];

Json
var persona ={
    nombre: "Sandra",
    edad: 30
}


Funciones: Conjunto de sentencias para realizar ciertas acciones 
con las variables.

Existen dos tipos de funciones:

1. Declarativas: por la palabra reservada se le llama declarativa.
    function miFuncion(){
        return 3;
    }

2. Expresión
Las variables pueden guardar otros tipos de valores, estos pueden ser Funciones.
Se conocen tambien como funciones anónimas, puesto que la función como tal no tiene
nombre, sino la variable. Aunque las funciones anónimas se les puede definir un nombre

var miFuncion = function(a, b){
    return a + b;
}

*****
SCOPE
*****
Alcance de las variables en un código.

*********
HOISTING
*********
El hoisting es cuando las variables (var) y funciones (function) se declaran antes que 
se procese cualquier tipo de código.
El hoisting solo pasa con versiones pasadas de JS de ECMASScript 5 hasta atrás.

Esto solo pasa con dos palabras clave: var y function.

Desde ECMASScript 6 se introdujeron nuevas palabras clave LET y CONSTANT.

*********
COERCION
*********
Poder cambiar el tipo de un valor. Puede ser implicita (el mismo motor lo hace) 
o Explicita haciendolo el desarrollador.

(+): El operador mas en la coerción se entiende como concatenación.
(*): El operador de multiplicación se entiende como multiplicación.

******************
ARROW FUNTIONS
******************

//A una variable se le puede asignaruna funcion en JS. 
//Es totalmente válido.
/*
const esMayorDeEdad = function (persona){ //funcion anónima
    return  persona.edad >= MAYORIA_DE_EDAD
}

//Otra forma de escribir esto es con Arrow Funtion

const esMayorDeEdad = (persona) => {
    return  persona.edad >= MAYORIA_DE_EDAD
}

*/

/** Arrow Funtion
 Se pueden omitir los paréntesis y se entiende que 
es el nombre de la funcion persona => 
const esMayorDeEdad = persona => {
    return  persona.edad >= MAYORIA_DE_EDAD
} */

/**
 * Si la función solo retorna algo se puede acortar más quitando
 * la palabra return y los paréntesis dejando todo en una sola línea.
   
    const esMayorDeEdad = persona => persona.edad >= MAYORIA_DE_EDAD
 */

/*
* Se puede desestructurar la función y sigue funcionando de igual forma 
*/