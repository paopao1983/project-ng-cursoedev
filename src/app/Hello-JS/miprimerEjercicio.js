
//declaracion de una var en Jtsc puro

/*    

    function addFavoriteBook(bookName){
        
        if(!bookName.includes("Great")){
            favoriteBooks.push(bookName);
        }

    }

    function printFavoriteBooks(){
        console.log(
            `Favorite Books: ${favoriteBooks.length} 
            `
            );
        
        for(let bookName of favoriteBooks){
            console.log(bookName);
        }
    }

    function addAstrudent(numStudent){
        return numStudent + 1;
    }
    
    var favoriteBooks = [];

    addFavoriteBook("A Song of Ice anf Fire");
    addFavoriteBook("The Great Gatsby");
    addFavoriteBook("Crime & Punishment");
    addFavoriteBook("Great Expectations");
    addFavoriteBook("You Don't Know JS");
    printFavoriteBooks();

    var r
    console.log(typeof r);
    
    r = 5;
    console.log(typeof r);
    
    r = "5";
    console.log(typeof r);

    console.log(typeof undefined);

    r = {age: 36};
    console.log(typeof r);

    r = null;
    console.log(typeof r);

    r = [1, 2, 3];
    console.log(typeof r);

    var v5 = 5

    var som = 5/"joa"

    console.log(Number.isNaN(som))

    //NEW
    var yesterday = new Date("February 16, 2020")
    console.log(yesterday.toUTCString())

    //var myGPA = String(transcript.gpa);
   // console.log(myGPA)

    console.log("Esto es Implicit Coercion : " + v5)

    console.log(`
    Esto es un ejemplo de Interpolation y coercion: ${v5}+"" ... lo ves?`)

    console.log(addAstrudent(Number("16")))

    var nombre1 = "sandra"
    var nombre2 = 'sandra'
    var variable1 = "133"
    var variable2 = 133

    console.log(nombre1 == nombre2)
    console.log(nombre1 === nombre2)

    console.log(variable1 == variable2)
    console.log(variable1 === variable2)
/*
var ids = people.map(person => person.id);

var ids = people.map(function getId(person){
	return person.id
});

getPerson()
.then(person => getData(person.id))
.then(renderData);

getPerson()
.then(function getDataFrom(person){
    return getData(person.id);
})
.then(renderData);*/

var teacher = "Sandra";

(function changeTeacher(){
    var teacher2 = "Alfre"
    console.log(`este es el profe: ${teacher}`);
})();

console.log(`este es el profe 2 : ${teacher}`);


function funclosure1(persona){
    
    setTimeout(function waitSec(){
        console.log(persona)
    },100)
    
}
funclosure1("Sandra castro")


function funclosure2(question){   
   return function holdYourQuestion(){  
       console.log(question + " " + this.asoc)
   };
    
}
 
var myquestion = funclosure2("Que es Closure");

myquestion();


function ask(question){   
    console.log(this.teacher, this.topic, question)     
 }
  
 function otherClass(){
     var myContext = {
         teacher: "Suzy",
         topic: "Sociology"
     };

     ask.call(myContext, "Why this exists... ");
     /** call see llama a una funcion a la cual se le pueden enviar 
      * otros parametros (contexto) además de sus parámetros, y a 
      * través de la keyword this, JS tiene la capacidad de identificar
      * las variables pasadas por parámetros que no son propias del método
      * llamado.**/
 }

 otherClass();

 var workshop = {
    teacher: "kyle",
    ask(question)
    {
        console.log(this.teacher, question)
    }
};

workshop.ask("What is implicit binding?");

//Prototypes

function myPrototype(developer){
    this.name = developer;
}

myPrototype.prototype.ask = function(pregunta){
    console.log(this.name, pregunta);
}

var juniorDev = new myPrototype("\n Remberto");

juniorDev.ask("Que es prototipo en JS?\n");
