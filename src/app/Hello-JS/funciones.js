const MAYORIA_DE_EDAD = 18;
var fulano = {
    nombre: "Sandra",
    apellido: "Castro",
    edad: 36,
    ingeniero: true,
    cocinero: true,
    cantante: false,
    dj: false,
    fotografo: true
}

var alguien = {
    nombre: "Nico",
    apellido: "Castro",
    edad: 2,
    ingeniero: false,
    cocinero: false,
    cantante: false,
    dj: false,
    fotografo: false
}

function esMayorDeEdad(persona)
{
    return  persona.edad >= MAYORIA_DE_EDAD
}

function imprimirSiEsMayorDeEdad(persona){
    
    if(esMayorDeEdad(persona))
        console.log(`${persona.nombre} es *mayor* de Edad`)
    else
        console.log(`${persona.nombre} es *menor* de Edad`)
}

