import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { MiPrimerComponent } from './miPrimeracrchivo/miprimer';
import { Miprimero2Component } from './miprimero2/miprimero2.component'

@NgModule({
  declarations: [
    AppComponent,
    MiPrimerComponent,
    Miprimero2Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
