class workshop{

    constructor(teacher){
        this.teacher = teacher;
    }
    ask(question){
        console.log(this.teacher, question);
    }
}

var deepJS = new workshop("kyle");
var reactJS = new workshop("suzy");

deepJS.ask("Is 'Class' a class?");
reactJS.ask("Is this class OK?");